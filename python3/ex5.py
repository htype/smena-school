import math

class Circle:
    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.radius = radius

    def __str__(self):
        return 'Circle at ({0}, {1}), radius: {2}'.format(self.x, self.y, self.radius)

    def print_diameter(self):
        print('diameter: {0}'.format(2 * self.radius))

    def print_length(self):
        print('length: {0}'.format(2 * math.pi * self.radius))

    def print_square(self):
        print('square: {0}'.format(math.pi * (self.radius ** 2)))

c = Circle(1, 1, 2)
print(c)
c.print_diameter()
c.print_length()
c.print_square()
