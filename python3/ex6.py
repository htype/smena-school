import datetime

class Student:
    def __init__(self, id, name, date, *courses):
        self.id   = id
        self.name = name
        self.date = date
        self.courses = list(courses)

    def __str__(self):
        out_str  = 'Id: {0}\nName: {1}\nBirthday: {2} \n'.format(self.id, self.name, self.date)
        out_str += 'Number of courses {0}\n'.format(len(self.courses))
        out_str += '{0:<20} {1}:\n'.format("Course", "Lecture Hall")
        for i in self.courses:
            out_str += '{0:<20}'.format(i.name)
            out_str += '{0}'.format(i.lecture_hall)
            out_str += '\n'
        return out_str

    def add_course(self, course):
        if not course in self.courses:
            course.add_student(self)

    def remove_course(self, course):
        if course in self.courses:
            course.remove_course(self)


class Course:
    def __init__(self, name, lecture_hall, *students):
        self.name = name
        self.lecture_hall = lecture_hall
        self.students = list(students)

    def __str__(self):
        out_str = '{0} (lecture hall: {1})\n'.format(self.name, self.lecture_hall)
        out_str += 'Number of students: {0}\n'.format(len(self.students))

        if len(self.students):
            out_str += 'Students:\n'
            out_str += '{0:<4} {1:<24} {2}:\n'.format("Id", "Name", "Birthday")

        for i in self.students:
            out_str += '{0:<5}'.format(i.id)
            out_str += '{0:<25}'.format(i.name)
            out_str += '{0}'.format(i.date)
            out_str += '\n'

        return out_str

    def add_student(self, student):
        if not student in self.students:
            student.courses.append(self)
            self.students.append(student)

    def remove_student(self, student):
        if student in self.students:
            student.courses.remove(self)
            self.students.remove(student)


jack   = Student(1, 'Jack Larson', datetime.date(2000, 1, 2))
bender = Student(2, 'Bender Rodriges', datetime.date(2010, 12, 3))
print(jack)
print(bender)

print('\n')
math = Course("Mathematics", 233)
phys = Course("Physics", 2, bender)

print('Before\n')
print(math)
print(phys)

math.add_student(jack)
math.add_student(jack)
math.remove_student(jack)
math.remove_student(jack)
math.add_student(jack)
math.add_student(bender)

print('After\n')
print(math)
print(phys)
