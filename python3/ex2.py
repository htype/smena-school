class Rectangle(object):
    def __init__(self, lx, ly, rx, ry):
        self.lx = lx
        self.ly = ly
        self.rx = rx
        self.ry = ry

    def __str__(self):
        return 'Rectangle at ({0}, {1}), ({2}, {3})'.format(self.lx, self.ly, self.rx, self.ry)

    def _get_geometry(self):
        height = abs(self.ly - self.ry)
        width  = abs(self.rx - self.lx)
        return (height, width)

    def print_geometry(self):
        (height, width) = self._get_geometry()
        print("height: {0}, width: {0}".format(height, width))

    def print_perimeter(self):
        (height, width) = self._get_geometry()
        p = 2 * (height + width)
        print("perimeter: {0}".format(p))

    def print_square(self):
        (height, width) = self._get_geometry()
        s = height * width
        print("square: {0}".format(s))

    def is_square(self):
        (height, width) = self._get_geometry()
        return height == width


rec = Rectangle(0, 1, 1, 0)
print(rec)
rec.print_geometry()
rec.print_perimeter()
rec.print_square()
print(rec.is_square())
