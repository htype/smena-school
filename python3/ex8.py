class Building(object):
    def __init__(self, south, west, width_WE, width_NS, height = 10):
        self.south = south
        self.west  = west
        self.width_WE = width_WE
        self.width_NS = width_NS
        self.height = height

    def corners(self):
        east = self.west + self.width_WE
        north = self.south + self.width_NS
        north_west = (north     , self.west)
        north_east = (north     , east)
        south_west = (self.south, self.west)
        south_east = (self.south, east)
        result = {
            'north-west': north_west,
            'north-east': north_east,
            'south_west': south_west,
            'south_east': south_east
        }
        return result

    def area(self):
        return self.width_NS * self.width_WE

    def volume(self):
        return self.area() * self.height

    def __repr__(self):
        return "Building({0},{1},{2},{3},{4})".format(self.south, self.west, self.width_WE, self.width_NS, self.height)

cucumber = Building(1, 2, 20, 22)
print(cucumber)
print("corners: {0}".format(cucumber.corners()))
print("area: {0}".format(cucumber.area()))
print("volume: {0}".format(cucumber.volume()))
