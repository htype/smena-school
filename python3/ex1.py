class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return 'Point at {}, {}'.format(self.x, self.y)

p = Point(1, 2)
print(p)
