import datetime

class Student:
    def __init__(self, id, name, date, *courses):
        self.id   = id
        self.name = name
        self.date = date
        self.courses = list(courses)

    def __str__(self):
        out_str  = 'Id: {0}\nName: {1}\nBirthday: {2}\n'.format(self.id, self.name, self.date)
        out_str += 'Number of courses {0}\n'.format(len(self.courses))
        out_str += '{0:<20} {1}:\n'.format("Course", "Lecture Hall")
        for i in self.courses:
            out_str += '{0:<20}'.format(i.name)
            out_str += '{0}'.format(i.lecture_hall)
            out_str += '\n'
        return out_str

    def add_course(self, course):
        if not course in self.courses:
            course.add_student(self)

    def remove_course(self, course):
        if course in self.courses:
            course.remove_student(self)

class Teacher:
    def __init__(self, id, name, date, course=None):
        self.id   = id
        self.name = name
        self.date = date
        self.course = course

    def __str__(self):
        out_str  =  'Id: {0}\n'.format(self.id)
        out_str  += 'Name: {0}\n'.format(self.name)
        out_str  += 'Birthday: {0}\n'.format(self.date)
        if (self.course):
            out_str  += 'Course: {0}'.format(self.course)
        else:
            out_str += 'has not courses'
        return out_str

    def set_course(course):
        course.set_lector(self)

    def unset_course():
        self.course.unset_lector(self)


class Course:
    def __init__(self, name, lecture_hall, *students):
        self.name = name
        self.lecture_hall = lecture_hall
        self.students = list(students)
        self.lector = None

    def __str__(self):
        out_str = '{0} (lecture hall: {1})\n'.format(self.name, self.lecture_hall)
        out_str += 'Lector: {0}\n'.format(self.lector.name)
        out_str += 'Number of students: {0}\n'.format(len(self.students))

        if len(self.students):
            out_str += 'Students:\n'
            out_str += '{0:<4} {1:<24} {2}:\n'.format("Id", "Name", "Birthday")

        for i in self.students:
            out_str += '{0:<5}'.format(i.id)
            out_str += '{0:<25}'.format(i.name)
            out_str += '{0}'.format(i.date)
            out_str += '\n'

        return out_str

    def add_student(self, student):
        if not student in self.students:
            student.courses.append(self)
            self.students.append(student)

    def remove_student(self, student):
        if student in self.students:
            student.courses.remove(self)
            self.students.remove(student)

    def set_lector(self, lector):
        if (self.lector):
            self.lector.course = None

        self.lector = lector
        lector.course = self

    def unset_lector(self, lector):
        if (self.lector):
            self.lector.course = None
            self.lector = None

jack   = Student(1, 'Jack Larson', datetime.date(2000, 1, 2))
bender = Student(2, 'Bender Rodriges', datetime.date(2010, 12, 3))
print(jack)
print(bender)

lorentz = Teacher(1, "Hendrik Antoon Lorentz", datetime.date(1853, 7, 18))
dijkstra = Teacher(2, "Edsger Wybe Dijkstra", datetime.date(1930, 5, 11))
print(lorentz)
print(dijkstra)

print('\n')
math = Course("Mathematics", 233)
math.set_lector(dijkstra)

phys = Course("Physics", 2,  bender)
phys.set_lector(lorentz)

print('Before\n')
print(math)
print(phys)

math.add_student(jack)
math.add_student(jack)
math.remove_student(jack)
math.remove_student(jack)
math.add_student(jack)
math.add_student(bender)

print('After\n')
print(math)
print(phys)
