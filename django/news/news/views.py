#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render_to_response

import datetime
import locale

def get_cur_date(request):
    loc = locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
    cur_date = datetime.datetime.now()
    str_date = cur_date.strftime("%d %B %Y")
    return HttpResponse(str_date)

def get_date_by_weekday(request):
    weekday = request.GET["weekday"]
    (year, week_num, _day) = datetime.datetime.now().isocalendar()
    date_str = "{0} {1} {2}".format(weekday, week_num, year)
    new_date = datetime.datetime.strptime(date_str, "%A %W %Y")
    return HttpResponse("{}".format(new_date.strftime("%d.%m.%Y")))

def get_weekday_by_date(request):
    str_date = request.GET["date"]
    date = datetime.datetime.strptime(str_date, "%d.%m.%Y")
    return HttpResponse(datetime.datetime.strftime(date, "%A"))

def get_diff_date(request):
    str_start = request.GET["date-begin"]
    str_end = request.GET["date-end"]
    start_date = datetime.datetime.strptime(str_start, "%d.%m.%Y")
    end_date = datetime.datetime.strptime(str_end, "%d.%m.%Y")
    diff = abs((end_date - start_date).days)
    return HttpResponse("{}".format(diff))

def get_taxes(type, salary):
    taxes = {
        'pid': 0.13,
        'pension': 0.22,
        'social_insurance': 0.029,
        'health_insurance': 0.029,
        'accident_insurance': 0.051
    }

    return taxes[type] * salary

def get_pid(request):
    salary = float(request.GET["salary"])
    tax = get_taxes("pid", salary)
    return HttpResponse("{}".format(tax))

def get_pension(request):
    salary = float(request.GET["salary"])
    tax = get_taxes("pension", salary)
    return HttpResponse("{}".format(tax))

def get_social_insurance(request):
    salary = float(request.GET["salary"])
    tax = get_taxes("social_insurance", salary)
    return HttpResponse("{}".format(tax))

def get_health_insurance(request):
    salary = float(request.GET["salary"])
    tax = get_taxes("health_insurance", salary)
    return HttpResponse("{}".format(tax))

def get_accident_insurance(request):
    salary = float(request.GET["salary"])
    tax = get_taxes("accident_insurance", salary)
    return HttpResponse("{}".format(tax))

def get_all_taxes(request):
    salary = float(request.GET["salary"])
    pension = get_taxes("pension", salary)

    social_insurance = get_taxes("social_insurance", salary)
    health_insurance = get_taxes("health_insurance", salary)
    accident_insurance = get_taxes("accident_insurance", salary)
    in_total = pension + social_insurance + health_insurance + accident_insurance

    out_str = u"<pre>"
    out_str += u"Пенсионный фонд: {}\n".format(pension, salary)
    out_str += u"Фонд социального страхования: {}\n".format(social_insurance, salary)
    out_str += u"Федеральный фонд обязательного медицинского страхования: {}\n".format(health_insurance, salary)
    out_str += u"Страховой налог от несчастного случая на производстве для служащих: {}\n".format(accident_insurance, salary)
    out_str += u"Итог: {}".format(in_total)
    out_str += u"</pre>"
    return HttpResponse(out_str)

def generate_site(request):
    return render_to_response(u"index.html")
