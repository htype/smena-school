"""news URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from .views import get_cur_date, get_date_by_weekday, get_weekday_by_date, get_diff_date, get_pid
from .views import get_pension, get_social_insurance, get_health_insurance, get_accident_insurance, get_all_taxes, generate_site

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^date/today/', get_cur_date),
    url(r'^date-by-weekday/', get_date_by_weekday),
    url(r'^weekday-by-date/', get_weekday_by_date),
    url(r'^date-range/', get_diff_date),
    url(r'^taxes/$', get_all_taxes),
    url(r'^taxes/pid', get_pid),
    url(r'^taxes/pension', get_pension),
    url(r'^taxes/social_insurance', get_social_insurance),
    url(r'^taxes/health_insurance', get_health_insurance),
    url(r'^taxes/accident_insurance', get_accident_insurance),
    url(r'^news/', generate_site),
] + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
