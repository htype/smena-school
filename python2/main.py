from ex.ex1 import ex1_test
from ex.ex2 import ex2_test
from ex.ex3 import ex3_test
from ex.ex4 import ex4_test
from ex.ex5 import ex5_test
from ex.ex6 import ex6_test
from ex.ex7 import ex7_test

def get_menu():
    menu = {
        1: {
            'desc': 'Product of sum of even list elements and the last element',
            'func': ex1_test
            },
        2: {
            'desc': 'Collect capital letters',
            'func': ex2_test
            },
        3: {
            'desc': 'Check whether there are 3 words in a row',
            'func': ex3_test
            },
        4: {
            'desc': 'The difference between the maximum and minimum',
            'func': ex4_test
            },
        5: {
            'desc': 'Count the number of primes up to 300',
            'func': ex5_test
            },
        6: {
            'desc': 'Calculate the difference between two dates',
            'func': ex6_test
            },
        7: {
            'desc': 'Print the dialog',
            'func': ex7_test
            }
    }

    return menu


def show_menu():
    menu = get_menu()
    for i in menu:
        print("{}) {}".format(i, menu[i]['desc']))

    task_num = int(input('Input task number (1-{}): '.format(len(menu))))
    print("task number: {}\n".format(task_num))

    work = menu[task_num]['func']
    work()

if __name__ == "__main__":
    show_menu()
