def contain_3_words_in_a_row(text):
    words = text.split(' ')
    words = filter(lambda i: i, words)

    words_in_row = 0
    for i in words:
        if (i.isalpha()):
            words_in_row += 1
        else:
            words_in_row = 0

        if (words_in_row == 3):
            return True

    return False

def ex3_test():
    print("contain_three_words_in_a_row('My name is Slim Shady') = {}".format(contain_3_words_in_a_row('My name is Slim Shady')))
    print("contain_three_words_in_a_row('') = {}".format(contain_3_words_in_a_row('')))
    print("contain_three_words_in_a_row('1 dog 2 cat juice') = {}".format(contain_3_words_in_a_row('1 dog 2 cat juice')))
