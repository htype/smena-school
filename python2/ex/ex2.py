def collect_upper(text):
    passwd = filter(lambda i: i.isupper(), text)
    return (passwd)

def ex2_test():
    print("collect_upper('Hello, Slim Shady') = '{}'".format(collect_upper('Hello, Slim Shady')))
    print("collect_upper('NYC') = '{}'".format(collect_upper('NYC')))
    print("collect_upper('') = '{}'".format(collect_upper("")))
