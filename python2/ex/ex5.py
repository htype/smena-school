import math

def count_primes(n):
    if n < 2:
        return 0

    n_primes = 0
    for i in range(2, n + 1):
        is_div = False
        for j in range(2, int(math.sqrt(i))):
            if i % j == 0 and j != i:
                is_div = True
                break

        if not is_div:
            n_primes += 1

    return n_primes

def ex5_test():
    print("count_primes(3) = {}".format(count_primes(3)))
    print("count_primes(300) = {}".format(count_primes(300)))
