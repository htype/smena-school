def calculate_even_ind(l):
    if not l:
        return 0
    sum = 0
    for i in l[::2]:
        sum += i
    return sum * l[-1]

def ex1_test():
    print("calculate_even_ind([1, 2, 3, 4]) = {}".format(calculate_even_ind([1, 2, 3, 4])))
    print("calculate_even_ind([]) = {}".format(calculate_even_ind([])))
    print("calculate_even_ind([-1]) = {}".format(calculate_even_ind([-1])))
