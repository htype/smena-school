def max_min_diff(*args):
    if not args:
        return 0

    maximum = args[0]
    minimum = maximum

    for i in args:
        if (maximum < i):
            maximum = i
        if (minimum > i):
            minimum = i

    return maximum - minimum

def ex4_test():
    print("max_min_diff(1, 3, 4.8, 2.2, 1, 5, 10.00101, -1.1) = {}".format(max_min_diff(1, 3, 4.8, 2.2, 1, 5, 10.00101, -1.1)))
    print("max_min_diff(1, 2, 3) = {}".format(max_min_diff(1, 2, 3)))
    print("max_min_diff() = {}".format(max_min_diff()))
