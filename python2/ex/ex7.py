#! /usr/bin/env python
# -*- coding: utf-8 -*-# -*- coding: utf-8 -*-

def get_default():
    comments = []

    comment = {
        "id": 1,
        "parent_id": 0,
        "author": 'Гарри',
        "time": '15:00',
        "text": 'Как перевернуть человека вверх ногами?'
    }
    comments.append(comment)

    comment = {
        "id": 2,
        "parent_id": 1,
        "author": 'Гермиона',
        "time": '15:05',
        "text": 'Левикорпус'
    }
    comments.append(comment)

    comment = {
        "id": 3,
        "parent_id": 2,
        "author": 'Гарри',
        "time": '15:15',
        "text": 'Спасибо, сработало'
    }
    comments.append(comment)

    comment = {
        "id": 4,
        "parent_id": 0,
        "author": 'Рон',
        "time": '15:16',
        "text": 'Как спастись от левикорпуса?'
    }
    comments.append(comment)

    comment = {
        "id": 5,
        "parent_id": 4,
        "author": 'Полумна',
        "time": '15:17',
        "text": 'Либеракорпус',
    }
    comments.append(comment)

    comment = {
        "id": 6,
        "parent_id": 3,
        "author": 'Гермиона',
        "time": '15:22',
        "text": 'На ком пробуешь'
    }
    comments.append(comment)

    comment = {
        "id": 7,
        "parent_id": 4,
        "author": 'Джинни',
        "time": '15:25',
        "text": 'Кто тебя обижает?'
    }
    comments.append(comment)

    comment = {
        "id": 8,
        "parent_id": 7,
        "author": 'Рон',
        "time": '15:26',
        "text": 'Гарри, кто же еще?'
    }
    comments.append(comment)

    comment = {
        "id": 9,
        "parent_id": 6,
        "author": 'Гермиона',
        "time": '15:28',
        "text": 'А все поняла))'
    }
    comments.append(comment)

    return comments

def create_tree(comments):
    tree = dict()
    for i in comments:
        id = i['id']
        if (tree.get(id) == None):
            tree[id] = [i]

    for i in comments:
        pid = i['parent_id']
        if (tree.get(pid) == None):
            tree[pid] = [i]
        else:
            tree[pid].append(i)

    return tree

def print_comment(comment, indent):
    str_ind = '-' * indent
    print("{}[{}] {}:".format(str_ind, comment['time'], comment['author']))
    print("{}{}:".format(str_ind, comment["text"]))

def print_comment_tree(com_tree, cur, indent):
    start = 1

    if (cur == 0):
        start = 0

    for i in com_tree[cur][start:]:
       print_comment(i, indent)
       print_comment_tree(com_tree, i['id'], indent + 2)

def print_dialog(dialog = get_default()):
    tree = create_tree(dialog)
    print_comment_tree(tree, 0, 2)

def ex7_test():
    print_dialog()
