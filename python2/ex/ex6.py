import datetime

def days_diff(date1, date2):
    a = datetime.date(date1[0], date1[1], date1[2])
    b = datetime.date(date2[0], date2[1], date2[2])

    diff = (a - b).days
    return abs(diff)

def ex6_test():
    print("days_diff((1982, 4, 19), (1982, 4, 22)) = {}".format(days_diff((1982, 4, 19), (1982, 4, 22))))
    print("days_diff((2014, 1, 1), (2014, 8, 27)) = {}".format(days_diff((2014, 1, 1), (2014, 8, 27))))
