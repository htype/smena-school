def count_n_in_list(l, n = 2):
    return len(filter(lambda i: i == n, l))

print("count_n_in_list([], 2) = {}".format(count_n_in_list([], 2)))
print("count_n_in_list([2, 3, 4, -2, 10, 2]) = {}".format(count_n_in_list([2, 3, 4, -2, 10, 2])))
print("count_n_in_list([1], 3) = {}".format(count_n_in_list([1], 3)))
print("count_n_in_list([2,3,4,2], 4) = {}".format(count_n_in_list([2, 3, 4, 2], 4)))
