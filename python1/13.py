def count_includes(l, n = []):
    return len(filter(lambda i: i == n and type(i) == type(n), l))

print("count_includes([], 2) = {}".format(count_includes([], 2)))
print("count_includes([2, 3, 4, -2, 10, 2]) = {}".format(count_includes([2, 3, 4, -2, 10, 2])))
print("count_includes([1], 3) = {}".format(count_includes([1], 3)))
print("count_includes([2,3,4,2], 4) = {}".format(count_includes([2, 3, 4, 2], 4)))
print("count_includes([[],3,[5],(4, 5), []]) = {}".format(count_includes([[],3, [5], (4, 5), []])))
print("count_includes([[],3,[5],(4, 5), []], (4, 5)) = {}".format(count_includes([[],3,[5],(4, 5), []], (4, 5))))
