def ret_min(a, b):
    return a if a < b else b

print("ret_min(10, 2) = {}".format(ret_min(10, 2)))
print("ret_min(-10, 2) = {}".format(ret_min(-10, 2)))
