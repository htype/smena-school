def fact(n):
    if n <= 1:
        return 1
    acc = 1
    for i in range(1, n+1):
        acc *= i
    return acc 

print("fact(0) = {}".format(fact(0)))
print("fact(10) = {}".format(fact(10)))
print("fact(4) = {}".format(fact(4)))
