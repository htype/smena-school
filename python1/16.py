def is_leap_year(year):
    return year % 4 == 0 and year % 100 != 0 or year % 400 == 0

print("is_leap_year(1700) = {}".format(is_leap_year(1700)))
print("is_leap_year(1582) = {}".format(is_leap_year(1582)))
print("is_leap_year(2000) = {}".format(is_leap_year(2000)))
