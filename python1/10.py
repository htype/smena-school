def count_two_in_list(l):
    return len(filter(lambda i: i == 2, l))

print("count_two_in_list([]) = {}".format(count_two_in_list([])))
print("count_two_in_list([1]) = {}".format(count_two_in_list([1])))
print("count_two_in_list([2,3,4,2) = {}".format(count_two_in_list([2, 3, 4, 2])))
