def count_num_in_list(l, n):
    return len(filter(lambda i: i == n, l))

print("count_num_in_list([], 2) = {}".format(count_num_in_list([], 2)))
print("count_num_in_list([2], 2) = {}".format(count_num_in_list([2], 2)))
print("count_num_in_list([2,3,4,4], 4) = {}".format(count_num_in_list([2, 3, 4, 4], 4)))
