def sum_digits(n):
    n = abs(n)
    sum = 0
    while n > 0:
        sum += n % 10
        n //= 10
    return sum


print("sum_digits(0) = {}".format(sum_digits(0)))
print("sum_digits(1) = {}".format(sum_digits(1)))
print("sum_digits(10) = {}".format(sum_digits(10)))
print("sum_digits(-12) = {}".format(sum_digits(-12)))
print("sum_digits(-90932) = {}".format(sum_digits(-90932)))
