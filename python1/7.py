def last_digit(n):
    return abs(n) % 10

print("last_digit(4) = {}".format(last_digit(4)))
print("last_digit(420) = {}".format(last_digit(420)))
print("last_digit(-42) = {}".format(last_digit(-42)))
