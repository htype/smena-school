import math

def hypotenuse(a, b):
    return math.sqrt(a * a + b * b)

print("hypotenuse(3, 4) = {}".format(hypotenuse(3, 4)))
print("hypotenuse(5, -10) = {}".format(hypotenuse(5, -10)))
