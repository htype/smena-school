def square(a):
    return a ** 2 

print("square(2) = {}".format(square(2)))
print("square(-3) = {}".format(square(-3)))
