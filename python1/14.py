def longest_str(*args):
    d = {}
    for i in args:
        d[len(i)] = i
    return d[max(d.keys())]

print("longest_str("") = {}".format(longest_str("")))
print("longest_str('a', 'bc', 'adc') = {}".format(longest_str('a', 'bc', 'abc')))
print("longest_str('', '23fsedfbc', 'adc') = {}".format(longest_str('', '23fsedfbc', 'abc')))
