def min_num_len(a, b):
    return a if abs(a) < abs(b) else b

print("min_num_len(1, 10) = {}".format( min_num_len(1, 10)))
print("min_num_len(100, 10) = {}".format( min_num_len(100, 10)))
print("min_num_len(-100, 10) = {}".format(min_num_len(-100, 10)))
print("min_num_len(-1, 10) = {}".format(min_num_len(-1, 10)))
print("min_num_len(12, 10) = {}".format(min_num_len(12, 10)))
