def r_shift_list(l):
    if not l:
        return l

    last = l[-1]
    for i in range(len(l) - 1, 0, -1):
        l[i] = l[i-1]

    l[0] = last
    return l

print("r_shift_list([0, 1, 2, 3]) = {}".format(r_shift_list([0, 1, 2, 3])))
print("r_shift_list([]) = {}".format(r_shift_list([])))
print("r_shift_list([1]) = {}".format(r_shift_list([1])))
