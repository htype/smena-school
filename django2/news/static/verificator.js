function getCommentFormFields()
{
    fields = [
        {
            'name': 'Content',
            'id': 'comment-content',
            'required': true,
            'check': check_non_empty
        },
        {
            'name': 'Phone',
            'id': 'comment-author-phone',
            'required': false,
            'check': check_phone
        },
        {
            'name': "Email",
            'id': 'comment-author-email',
            'required': false,
            'check': check_email
        }
    ];

    return fields;
}

function getRegistrationFormFields()
{
    fields = [
        {
            'name': 'Username',
            'id': 'registration-username',
            'required': true,
            'check': check_non_empty
        },
        {
            'name': 'Password',
            'id': 'registration-password',
            'required': true,
            'check': check_password
        },
        {
            'name': "Phone",
            'id': 'registration-phone',
            'required': false,
            'check': check_phone
        },
        {
            'name': 'Email',
            'id': 'registartion-email',
            'required': true,
            'check': check_email
        }
    ];

    return fields;
}

function findFieldInForm(fieldName, fields)
{
    if (!fieldName) {
        return fields;
    }

    for (i in fields) {
        if (fields[i].name == fieldName) {
            return fields[i];
        }
    }
}

function setErrorText(container, errorText)
{
    var errorClass = "errorlist";
    var errorBlock = container.lastElementChild;
    if (errorClass != errorBlock.className) {
        var parentErrorElement = document.createElement('ul');
        var errorElement = document.createElement('li');
        errorElement.innerHTML = errorText;
        parentErrorElement.appendChild(errorElement);
        parentErrorElement.classList.add(errorClass);
        container.appendChild(parentErrorElement);
    } else {
        errorBlock.firstChild.innerHTML = errorText;
    }
}

function removeErrorText(container)
{
    var errorClass = "errorlist";
    var errorBlock = container.lastElementChild;
    if (!errorBlock || errorClass != errorBlock.className) {
        return;
    }

    container.removeChild(errorBlock);
}

function setStatus(form, is_fail) {
    var statusElement = form.getElementsByClassName("submit-status")[0];
    statusElement.innerHTML = is_fail ? "Fail" : "Success";
}

function check_email(email_as_string)
{
    return /^[\w\-\.]+@[\w\-]+\.[a-zA-z0-9]+$/.test(email_as_string);
}

function check_password(password_as_string)
{
    return /^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?:[a-zA-Z0-9]){6,10})$/.test(password_as_string);
}

function check_phone(phone_as_string)
{
    var num_groups = phone_as_string.match(/\d/g);
    if (!num_groups) {
        return false
    }

    var clean_phone = num_groups.slice(1).join("");
    if (clean_phone.length < 6 || clean_phone.length > 10) {
        return false
    }

    return true;
}

function check_non_empty(text)
{
    return text;
}

function verifyForm(fields)
{
    var errors = 0;
    for (i in fields) {
        errors += recheckField(fields[i]);
    }

    return errors;
}

function verifyCommentForm(form)
{
    var fields = getCommentFormFields();

    var errors = verifyForm(fields, errors);
    var emailField = document.getElementById(findFieldInForm('Email', fields).id);
    var phoneField = document.getElementById(findFieldInForm('Phone', fields).id);

    if (!emailField.value && !phoneField.value) {
        setErrorText(form, "Phone or email is required");
        errors++;
    }

    var hasErrors = errors ? true : false;
    setStatus(form, hasErrors);

    if (hasErrors) {
        return false;
    }

    form.submit();
    return true;
}

function verifyRegistrationForm(form)
{
    var fields = getRegistrationFormFields();

    var errors = verifyForm(fields, errors);

    var hasErrors = errors ? true : false;

    if (hasErrors) {
        return false;
    }

    form.submit();
}

function recheckField(field)
{
    var element = document.getElementById(field.id);
    removeErrorText(element.parentElement);
    removeErrorText(element.form);

    if (field.required && !check_non_empty(element.value))
    {
        setErrorText(element.parentElement, "This field is required");
        return 1;
    }

    if (element.value &&
        !field.check(element.value))
    {
        errorText = "Invalid " + field.name;
        setErrorText(element.parentElement, errorText);
    }

    return 0;
}

function recheckFieldByName(fieldName, getFieldFunction)
{
    var field = findFieldInForm(fieldName, getFieldFunction(fieldName));
    recheckField(field);
}
