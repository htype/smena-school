$(document).ready(function(){
    $(".calculator-operation").click(function(){
        argument1 = parseInt($(".calculator-argument-1").val(), 10);
        argument2 = parseInt($(".calculator-argument-2").val(), 10);

        result = 0;
        if ($(this).hasClass("calculator-operation-addition")) {
            result = argument1 + argument2;
        } else if ($(this).hasClass("calculator-operation-substraction")) {
            result = argument1 - argument2;
        } else if ($(this).hasClass("calculator-operation-multiplication")) {
            result = argument1 * argument2;
        } else if ($(this).hasClass("calculator-operation-division")) {
            // if you divide on 0, you get infinity
            result = argument1 / argument2;
        } else {
            result = NaN;
        }

        $(".calculator-result").val(result);
    });
});
