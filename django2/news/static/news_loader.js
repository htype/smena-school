function newsLoader(config) {

    function getNewsTitles() {
        $.ajax({
            type: "GET",
            url: config.getNewsTitlesUrl
        }).done(function(newsTitles) {
            if (newsTitles) {
                reprintTitles(newsTitles);
            }
        });

        return null;
    }

    function getBookmarks() {
        $.get(config.getBookmarksUrl).done(function(bookmarks){
            if (bookmarks) {
                reprintTitles(bookmarks);
            }
        });
    }

    function setNewsItemAction(in_bookmarks, is_authenticated) {
        if (!is_authenticated) {
            return;
        }

        var action = (in_bookmarks) ? deleteBookmark : createBookmark;
        var actionTitle = (in_bookmarks) ? "Delete bookmark" : "Create bookmark";
        $(".news-list-header-item__create-bookmark").off();
        $(".news-list-header-item__create-bookmark").click(action);
        $(".news-list-header-item__create-bookmark").text(actionTitle);
    }

    function getNewsTitleDict() {
        title = $.trim($(".news-list-header-item__title").text());
        return {
            title: title
        };
    }

    function createBookmark() {
        var titleDictionary = getNewsTitleDict();
        titleDictionary["action"] = "create"
        var formData = $(".one-page-application").serialize();

        var ajaxData = formData + "&" + $.param(titleDictionary);
        $.post(config.createBookmarkUrl, ajaxData, function(data) {
            if (data.status == "Success") {
                setNewsItemAction(true, true);
            }
        });
    }

    function deleteBookmark() {
        var titleDictionary = getNewsTitleDict();
        titleDictionary["action"] = "delete"
        var formData = $(".one-page-application").serialize();

        var ajaxData = formData + "&" + $.param(titleDictionary);
        $.post(config.deleteBookmarkUrl, ajaxData, function(data) {
            if (data.status == "Success") {
                setNewsItemAction(false, true);

            }
        });
    }

    function printNewsItemText(title, content, in_bookmarks, is_authenticated) {
        setNewsItemAction(in_bookmarks, is_authenticated);
        $(".news-list-header-item__title").text(title);
        $(".news-item-body").html(content);
    }

    function loadNewsItemText() {
        var slug = $(this).attr("href").slice(1);
        var newsItemUrl = config.getNewsItemUrl;
        $.ajax({
            type: "GET",
            url:  newsItemUrl,
            data: {"slug": slug}
        }).done(function(newsItemText) {
            printNewsItemText(newsItemText.name, newsItemText.content,
                              newsItemText.in_bookmarks, newsItemText.is_authenticated);
        });
    }


    function reprintTitles(titles) {
        var newsListBody = $(".news-list-body");
        newsListBody.empty();
        for (i in titles) {
            var href = '#' + titles[i].slug;
            var titleElement = "<a href='" + href + "' class='news-title'>" + titles[i].name + "</a>";
            newsListBody.append(titleElement);
        }

        $(".news-title").click(loadNewsItemText);
    }

    $(document).ready(function() {
        $(".news-list-header-item__all-news").click(function() {
            getNewsTitles();
        });

        $(".news-list-header-item__all-bookmarks").click(function() {
            getBookmarks();
        });
    });
};
