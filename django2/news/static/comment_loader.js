function commentLoader(config){

    function addComment(author_as_string, text) {
        var commentsBlock = $(".wrapper-content__comments");
        var commentText = author_as_string + ": " + text;
        commentsBlock.append("<div class='comment'>" + commentText + "</div>");
    }

    function reprintComments(comments) {
        $(".wrapper-content__comments").empty();

        for (i in comments) {
            comment = comments[i];
            addComment(comment.author, comment.text);
        }
    }

    function updateComments() {
        $.ajax({
            type: "GET",
            url:  config.getCommentsUrl
        }).done( function(comments) {
            reprintComments(comments)
            setTimeout(updateComments, 5000);
        })
    }

    $(document).ready(function() {

        $(".news-comment-form").submit(function(){
            if (verifyCommentForm(this)) {
                var slug = "&news_slug=" + config.newsSlug;
                $.ajax({
                    data: $(this).serialize() + slug,
                    type: "POST",
                    url:  config.createCommentUrl,
                    success: function(comment) {
                        addComment(comment.author, comment.text);
                    }
                });
            }
            return false;
        });

        updateComments();
    });
};
