function getRandomColor() {
    return "#"+((1<<24)*Math.random()|0).toString(16);
}

$(document).ready(function() {

    $(".color-changer__button.background-red").click(function(){
        $("body").css("background-color", "#733");
    });

    $(".color-changer__button.background-blue").click(function(){
        $("body").css("background-color", "#337");
    });

    $(".color-changer__button.background-green").click(function(){
        $("body").css("background-color", "#373");
    });

    $(".color-changer__button.background-random").click(function(){
        var randomColor = "#"+((1<<24)*Math.random()|0).toString(16);
        $("body").css("background-color", randomColor);
        $(this).css("background-color", randomColor);
    });
});
