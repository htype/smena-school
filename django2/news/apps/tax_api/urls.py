from django.conf.urls import url

from tax_api.views import get_cur_date, get_date_by_weekday, get_weekday_by_date, get_diff_date
from tax_api.views import get_all_taxes, get_pid, get_pension
from tax_api.views import get_social_insurance, get_health_insurance, get_accident_insurance

urlpatterns = [
    url(r'^date/today/', get_cur_date),
    url(r'^date-by-weekday/(?P<weekday>\w+)/$', get_date_by_weekday),
    url(r'^weekday-by-date/(?P<date>.+)/$', get_weekday_by_date),
    url(r'^date-range/(?P<date_begin>.+)/(?P<date_end>.+)/$', get_diff_date),
    url(r'^taxes/\{(?P<salary>\d+)\}/$', get_all_taxes),
    url(r'^taxes/pid/\{(?P<salary>\d+)\}/$', get_pid),
    url(r'^taxes/pension/\{(?P<salary>\d+)\}', get_pension),
    url(r'^taxes/social_insurance/\{(?P<salary>\d+)\}', get_social_insurance),
    url(r'^taxes/health_insurance/\{(?P<salary>\d+)\}', get_health_insurance),
    url(r'^taxes/accident_insurance/\{(?P<salary>\d+)\}', get_accident_insurance),
]
