from __future__ import unicode_literals

from django.apps import AppConfig


class TaxApiConfig(AppConfig):
    name = 'tax_api'
