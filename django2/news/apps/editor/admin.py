from django.contrib import admin

from editor.models import Editor

admin.site.register(Editor)
