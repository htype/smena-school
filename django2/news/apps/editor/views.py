from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic.list import ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from news.forms import NewsModelForm
from news.models import News

class EditorProfileView(ListView):
    model = News
    template_name = u"editor/editor_profile.html"
    context_object_name = "news"
    paginate_by = 5
    page_kwarg = "page"

    @method_decorator(login_required(login_url=reverse_lazy(u"login_url")))
    def dispatch(self, request, *args, **kwargs):
        return super(EditorProfileView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        editor = self.request.user.editor
        return News.objects.filter(author = editor)


class NewsCreateView(CreateView):
    model = News
    form_class = NewsModelForm
    success_url = reverse_lazy(u"editor_profile_url")
    template_name = u"editor/add_news.html"

    def get_form_kwargs(self):
        kwargs = super(NewsCreateView, self).get_form_kwargs()
        kwargs.update({
            "editor": self.request.user.editor
        })
        return kwargs


class NewsUpdateView(UpdateView):
    model = News
    form_class = NewsModelForm
    slug_field = u"slug"
    slug_url_kwarg = u"news_item"
    success_url = reverse_lazy(u"editor_profile_url")
    template_name = u"editor/edit_news.html"

    def get_form_kwargs(self):
        kwargs = super(NewsUpdateView, self).get_form_kwargs()
        kwargs.update({
            "editor": self.request.user.editor
        })
        return kwargs
