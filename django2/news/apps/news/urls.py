"""news URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static

from .views import IndexView
from .views import NewsView, NewsItemView
from .views import RegisterView
from .views import UserProfileView
from .views import CalculatorView
from .views import LoginView
from .views import JsonCreateComment, JsonGetComments
from .views import JsonGetNewsTitles, JsonGetNewsItem
from .views import JsonCreateDeleteBookmark, JsonGetBookmarks
from .views import OnePageApplication
from editor.views import NewsUpdateView, NewsCreateView
from editor.views import EditorProfileView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tax_api/', include("tax_api.urls")),
    url(r'^calculator/$', CalculatorView.as_view(), name="calculator_url"),
    url(r'^site/$', IndexView.as_view(), name="site_url"),
    url(r'^site/news/$', NewsView.as_view(), name="site_news_url"),
    url(r'^site/news/(?P<news_item>\w+)/$', NewsItemView.as_view(), name = "news_item_url"),
    url(r'^site/register/$', RegisterView.as_view(), name = "register_url"),
    url(r'^site/user/logout/$', views.logout, name = "logout_url"),
    url(r'^site/user/login/$', LoginView.as_view(), name = "login_url"),
    url(r'^site/profile/$', UserProfileView.as_view(), name ="profile_url"),
    url(r'^site/editor_profile/$', EditorProfileView.as_view(), name = "editor_profile_url"),
    url(r'^site/editor_profile/(?P<page>\d+)/$', EditorProfileView.as_view(), name = "editor_profile_page_url"),
    url(r'^site/editor_profile/add_news/$', NewsCreateView.as_view(), name = "editor_profile_add_news_url"),
    url(r'^site/editor_profile/edit_news/(?P<news_item>\w+)/$', NewsUpdateView.as_view(), name = "editor_profile_edit_news_url"),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^one_page_application/$', OnePageApplication.as_view(), name = u"one_page_application_url"),
    url(r'^content/comments/create/$', JsonCreateComment.as_view(), name = u"create_comment_url"),
    url(r'^content/comments/get_all/$', JsonGetComments.as_view(), name = u"get_all_comments_url"),
    url(r'^content/news/get_news_titles/$', JsonGetNewsTitles.as_view(), name = u"get_news_titles_url"),
    url(r'^content/news/get_news_item/$', JsonGetNewsItem.as_view(), name = u"get_news_item_url"),
    url(r'^content/bookmarks/get_all/$', JsonGetBookmarks.as_view(), name = u"get_all_bookmarks_url"),
    url(r'^content/bookmarks/create/$', JsonCreateDeleteBookmark.as_view(), name = u"create_bookmark_url"),
    url(r'^content/bookmarks/delete/$', JsonCreateDeleteBookmark.as_view(), name = u"delete_bookmark_url"),
] + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
