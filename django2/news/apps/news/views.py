#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import json

from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render
from django.utils.timezone import utc
from django.views.generic.base import TemplateView, View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView, BaseDetailView
from django.views.generic.edit import CreateView, FormView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from news.models import News, Comments, Bookmark
from news.forms import RegisterForm, CommentsModelForm, LoginModelForm
from news.forms import NewsModelForm


class IndexView(TemplateView):
    template_name = u"index.html"

class NewsView(ListView):
    model = News
    template_name = u"news.html"
    context_object_name = u"news"

class NewsItemView(DetailView):
    model = News
    context_object_name = u"news_item"
    slug_field = u"slug"
    slug_url_kwarg = u"news_item"
    template_name = u"news_item.html"
    form_class = CommentsModelForm

    def get_context_data(self, **kwargs):
        top_the_best = News.objects.all().order_by("-views")[:10]
        top_the_newest = News.objects.all().order_by("-date_of_creating")[:10]
        all_news = News.objects.all()
        top_the_most_discused = sorted(all_news, key = lambda a: a.comments_set.count(), reverse=True)

        context = super(NewsItemView, self).get_context_data(**kwargs)
        context[u"comments"] = Comments.objects.filter(news = self.get_object())
        context[u"top_the_newest"] = top_the_newest,
        context[u"top_the_best"] = top_the_best,
        context[u"top_the_most_discufed"] = top_the_most_discused,
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.views += 1
        self.object.save()

        context = self.get_context_data()
        context.update({
            "comments_form": CommentsModelForm()
        })

        return render(request, self.template_name, context)


    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        user = request.user if request.user.is_authenticated else None;
        comments_form = CommentsModelForm(self.object, user, request.POST)

        status = u"Fail"
        if comments_form.is_valid():
            status = u"Success"
            comments_form.save()

        context = self.get_context_data()
        context.update({
            u"comments_form": comments_form,
            u"status": status,
        });

        return render(request, self.template_name, context)


class RegisterView(FormView):
    form_class = RegisterForm
    template_name = u"register.html"

    def form_valid(self, form):
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        email    = form.cleaned_data["email"]
        User.objects.create_user(username, password=password, email=email)

        redirect_url = self.request.GET["next"]
        if redirect_url:
            return HttpResponseRedirect(redirect_url)
        else:
            return HttpResponseRedirect(reverse(u"site_url"))

class UserProfileView(TemplateView):
    template_name = u"user_profile.html"

    def get_context_data(self, **kwargs):
        user = self.request.user
        if (user.is_authenticated()):
            context = super(UserProfileView, self).get_context_data(**kwargs)
            time_since_registration = datetime.datetime.now().replace(tzinfo=None) - user.date_joined.replace(tzinfo=None)
            comments_count = user.comments_set.count()
            time_difference = datetime.datetime.utcnow().replace(tzinfo = utc) - user.date_joined
            if (time_difference.days == 0):
                comments_per_day = comments_count
            else:
                comments_per_day = comments_count / time_difference.days
            context.update({
                            "time_since_registration": time_since_registration,
                            "comments_per_day": comments_per_day,
                        })
        else:
             context = dict()
        return context


class LoginView(FormView):
    form_class = LoginModelForm
    template_name = u"login.html"
    success_url = reverse_lazy(u"site_url")

    def form_valid(self, form):
        username = form.cleaned_data["login"]
        password = form.cleaned_data["password"]
        user = authenticate(username=username, password=password)
        if user:
            login(self.request, user)
            redirect_url = self.request.GET.get('next')
            if redirect_url:
                return HttpResponseRedirect(redirect_url)
        else:
            form.add_error(None, "Invalid login or password")
            context = self.get_context_data()
            context["form"] = form
            return render(self.request, self.template_name, context)

        return super(LoginView, self).form_valid(form)


class CalculatorView(TemplateView):
    template_name = u"calculator.html";


class JsonResponseMixin(object):
    def render_to_json_response(self, context, **response_kwargs):
        return JsonResponse(context, safe=False)

class JsonCreateComment(JsonResponseMixin, CreateView):
    model = Comments
    form_class = CommentsModelForm

    def form_valid(self, form):
        request = self.request
        user = request.user if request.user.is_authenticated else None;
        news_slug = request.POST.get("news_slug")
        news_item = News.objects.get(slug = news_slug)
        form = CommentsModelForm(request.POST, news_item = news_item, user = user)
        form.save()
        form_data = form.data.dict()
        form_data["author"] = user.username if user else "Anonimous"
        return self.render_to_json_response(form_data)


class JsonGetComments(JsonResponseMixin, ListView):
    def get_queryset(self):
        return [{
            "text": comment.text,
            "author": comment.user.username if comment.user else "Anonimous"
        } for comment in Comments.objects.all()]

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(self.object_list)


class OnePageApplication(TemplateView):
    template_name = u"one_page_application.html"


class JsonGetNewsTitles(JsonResponseMixin, ListView):
    def get_queryset(self):
        return [ news_item for news_item in News.objects.values("name", "slug") ]

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(self.object_list)


class JsonGetNewsItem(JsonResponseMixin, DetailView):
    def get_object(self, queryset=None):
        slug = self.request.GET["slug"]
        news_item = News.objects.get(slug=slug)
        user = self.request.user
        in_bookmarks = False
        if user.is_authenticated:
            in_bookmarks = Bookmark.objects.filter(news=news_item, user=user).exists()
        news_item_dict = {
            "name": news_item.name,
            "content": news_item.content,
            "in_bookmarks": in_bookmarks,
            "is_authenticated": user.is_authenticated(),
        }
        return news_item_dict

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(self.object)

def create_bookmark_dict(bookmark):
    if (bookmark):
        return  {
            "name": bookmark.news.name,
            "slug": bookmark.news.slug,
        }
    return None

class JsonGetBookmarks(JsonResponseMixin, ListView):
    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            bookmarks = Bookmark.objects.filter(user=user)
            bookmarks_dict = [create_bookmark_dict(bookmark) for bookmark in bookmarks]
            return bookmarks_dict
        return None

    def render_to_response(self, context, **response_kwargs):
        return self.render_to_json_response(self.object_list)

from django.core.exceptions import ObjectDoesNotExist

class JsonCreateDeleteBookmark(JsonResponseMixin, View):
    def post(self, request, *args, **kwargs):
        user = self.request.user
        if not user.is_authenticated:
            return self.send_status(False)

        news_title = request.POST["title"]
        try:
            news_item = News.objects.get(name = news_title)
        except ObjectDoesNotExist:
            return self.send_status(False)

        action = request.POST["action"]
        if action == "create":
            return self.get_or_create_bookmark(news_item, user)
        elif action == "delete":
            return self.delete_bookmark(news_item, user)
        return self.send_status(False)

    def get_or_create_bookmark(self, news_item, user):
        new_bookmark, created = Bookmark.objects.get_or_create(
            news = news_item,
            user = user,
        )
        bookmark_dict = create_bookmark_dict(new_bookmark)
        status_dictionary = self.get_status_dictionary(True)
        bookmark_dict.update(status_dictionary)
        return self.render_to_json_response(bookmark_dict)

    def delete_bookmark(self, news_item, user):
        Bookmark.objects.filter(news=news_item, user=user).delete()
        return self.send_status(True)

    def get_status_dictionary(self, is_success):
        status_as_text = "Success" if is_success else "Fail"
        status_dictionary = {
            "status": status_as_text,
        }
        return status_dictionary

    def send_status(self, is_success):
        status_dictionary = self.get_status_dictionary(is_success)
        return self.render_to_json_response(status_dictionary)
