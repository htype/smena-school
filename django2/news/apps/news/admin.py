from django.contrib import admin
from news.models import News, Users, Comments, Bookmark


class CommentsModelAdmin(admin.ModelAdmin):
    list_display = ("show_news_name", "show_user_name", "text")

    def show_user_name(self, obj):
        if obj.user:
            return "{}".format(obj.user.username)
        else:
            return "-"

    def show_news_name(self, obj):
        if obj.news:
            return "{}".format(obj.news.slug)


admin.site.register(News)
admin.site.register(Users)
admin.site.register(Bookmark)
admin.site.register(Comments, CommentsModelAdmin)
