#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User as DjangoUser
from django.core.urlresolvers import reverse
from editor.models import Editor

from redactor.fields import RedactorField

import datetime
import re

class News(models.Model):
    name    = models.CharField(verbose_name = u'имя', max_length = 200)
    content = RedactorField(verbose_name = u"содержание", default="")
    slug    = models.SlugField(verbose_name = u'slug', unique=True)
    date_of_creating = models.DateTimeField(verbose_name = u'дата_создания', auto_now_add=True, null = True)
    date_of_editing  = models.DateTimeField(verbose_name = u'дата_изменения', auto_now=True, blank = True, null = True)
    image = models.ImageField(verbose_name = u'изображение', blank = True)
    views  = models.IntegerField(verbose_name = u'количество_просмотров', blank = True, default = 0)
    author = models.ForeignKey(Editor, null = True, blank = True, on_delete = models.CASCADE)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            generated_slug = self.author.user.username + " " + self.name
            clear_slug = "".join(re.findall("[\w\s]+", generated_slug, re.UNICODE))
            self.slug = clear_slug.replace(' ', '_')
        super(News, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("news_item_url", kwargs={"news_item": self.slug})

class Users(models.Model):
    name = models.CharField(verbose_name = u'имя', max_length = 200)
    surname = models.CharField(verbose_name = u'фамилия', max_length = 200)
    registration_date = models.DateTimeField(verbose_name = u'дата_регистрации', auto_now_add=True)

class Comments(models.Model):
    news  = models.ForeignKey(News, null = True, on_delete = models.CASCADE)
    user  = models.ForeignKey(DjangoUser, null = True, blank = True, on_delete = models.CASCADE)
    publication_date = models.DateTimeField(verbose_name = u'дата_регистрации', null = True, auto_now_add=True)
    text  = models.TextField(verbose_name = u'содержание', default="")
    communication = models.CharField(verbose_name = u'способ_связи', null=True, max_length = 200)

class Bookmark(models.Model):
    user = models.ForeignKey(DjangoUser, null = True, blank = True)
    news = models.ForeignKey(News, null = True, blank = True)
