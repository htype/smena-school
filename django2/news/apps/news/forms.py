from django import forms
from django.db import models
from news.models import Comments, News
from django.contrib.auth.models import User
from redactor.widgets import RedactorEditor

import re

class RegisterForm(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True)
    email    = forms.CharField(required=True)
    phone_number = forms.CharField(required=False)

    def clean_password(self):
        password = self.cleaned_data['password']
        clear_password = clean_strict_pass(password)
        if not clear_password:
            raise forms.ValidationError("Password is invalid")
        return clear_password.group()

    def clean_email(self):
        email = self.cleaned_data['email']
        clear_email = clean_email(email)
        if not clear_email:
            raise forms.ValidationError("Email is invalid")
        return clear_email.group()

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get(u'phone_number')
        if not phone_number:
            return u""
        clear_phone_number = clean_phone_number(phone_number)
        if not clear_phone_number:
            raise forms.ValidationError('Phone number is invalid')
        return clear_phone_number

class LoginModelForm(forms.Form):
    login = forms.CharField(required=True)
    password = forms.CharField(required=True)

class CommentsModelForm(forms.ModelForm):
    class Meta:
        model  = Comments
        fields = ['text']

    username = forms.CharField(required = False)
    email = forms.EmailField(required = False)
    phone = forms.CharField(required = False)

    def __init__(self, *args, **kwargs):
        self.news_item = kwargs.pop("news_item", None)
        self.user = kwargs.pop("user", None)
        super(CommentsModelForm, self).__init__(*args, **kwargs)

    def save(self):
        comment = super(CommentsModelForm, self).save(commit = False)
        comment.news = self.news_item
        comment.user = self.user
        comment.communication = self.cleaned_data.get(u'email') or self.cleaned_data.get(u'phone')
        comment.save()
        return comment

    def clean_phone(self):
        phone = self.cleaned_data.get(u"phone")
        if not phone:
            return u""
        clear_phone = clean_phone_number(phone)
        if not clear_phone:
            raise forms.ValidationError('Phone number is invalid')
        return clear_phone

    def clean(self):
        cleaned_data = super(CommentsModelForm, self).clean()
        if (not bool(cleaned_data.get('phone')) and not bool(cleaned_data.get('email'))):
            raise forms.ValidationError('Phone or email is required')
        return cleaned_data

def clean_strict_pass(password):
    return re.search("^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?:[a-zA-Z0-9]){6,10})$", password)

def clean_email(email):
    return re.search("^([\w\-\.]+@[\w\-]+\.[a-zA-z0-9]+)$", email)

def clean_phone_number(number):
    nums = re.findall("\d", number)
    if not nums:
        return None
    short_num = "".join(nums)[1:]
    if (not short_num or len(short_num) < 6 or len(short_num) > 10):
        return None
    return short_num

class NewsModelForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ['name', 'content']
        widgets = {
            'content': RedactorEditor()
        }

    def __init__(self, *args, **kwargs):
        self.editor = kwargs.pop("editor", None)
        super(NewsModelForm, self).__init__(*args, **kwargs)

    def save(self):
        news = super(NewsModelForm, self).save(commit = False)
        news.author = self.editor
        news.save()
        return news

    def clean(self):
        cleaned_data = super(NewsModelForm, self).clean()
        return cleaned_data
