#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

class Validator:
    def check_num_code(self, code):
        return bool((re.search("^\d{4,}$", code)))

    def check_lower_case_pass(self, password):
        return bool((re.search("^[a-z]{6,10}$", password)))

    def check_uppercase_pass(self, password):
        return bool((re.search("^[a-zA-Z0-9@._-]{6,10}$", password)))

    def check_strict_pass(self, password):
        return bool(re.search("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\@\.\-\_])([a-zA-Z0-9\@\.\-\_]){6,10}$", password))

class FruitCounter:
    def _count_friut(self, str, fruit):
        pattern = u"{}\:\s(\d+)".format(fruit)
        fruits = re.findall(pattern, str, re.UNICODE)
        count = reduce(lambda acc,i: acc + int(i), fruits, 0)
        return count

    def count_apples(self, str):
        return self._count_friut(str, u"яблок")

    def count_pears(self, str):
        return self._count_friut(str, u"груш")

    def count_melons(self, str):
        return self._count_friut(str, u"дынь")

    def count_all(self, str):
        return self._count_friut(str, u"\w+")

    def fruits(self, str):
        pattern = u"(\w+)\:\s(\d+)"
        fruits = re.findall(pattern, str, re.UNICODE)
        return dict(fruits)

def cut_phone_number(number):
    nums = re.findall("\d+", number)
    if (not nums):
        return None

    short_num = "".join(nums)[1:]
    return short_num

def check_email(email):
    return bool(re.search("^[\w\-]+@[\w\-]+\.[a-zA-z0-9]+$", email))

test = u"Яна съела яблок: 3. Аня съела яблок: 4. Андрей съел дынь: 5. Максим съел яблок: 2."
fc = FruitCounter()
print("apples = {}".format(fc.count_apples(test)))
print("pear = {}".format(fc.count_pears(test)))
print("melons = {}".format(fc.count_melons(test)))
print("all = {}".format(fc.count_all(test)))
print((u"fruits = {}".format(fc.fruits(test))))
